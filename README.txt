Drupal Buy Me a Beer README
---------------------------------
---------------------------------
This module requires Drupal 7.X.

Overview:
---------------------------------
This module adds a link to donate a beer via Paypal to nodes of activated types.

Installation and configuration:
---------------------------------
Simply extract the download package in your modules directory and 
then enable the module in 'admin/build/modules/'.

For configuration options go to
'admin/content/buymeabeer'

More information is available at:
http://www.seo-expert-blog.com/tools/cms/drupal/modules/buy-me-a-beer
HTML Link:
<a href="http://www.seo-expert-blog.com/tools/cms/drupal/modules/buy-me-a-beer">Buy Me a Beer</a>